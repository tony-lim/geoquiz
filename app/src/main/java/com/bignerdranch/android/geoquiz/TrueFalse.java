package com.bignerdranch.android.geoquiz;

/**
 * Created by carvantes on 11/8/14.
 */
public class TrueFalse {
    private int mQuestion; // store resourceID
    private boolean mTrueQuestion; //whether statement true or false

    public TrueFalse(int question, boolean trueQuestion) {
        mQuestion = question;
        mTrueQuestion = trueQuestion;
    }

    public int getQuestion() {
        return mQuestion;
    }

    public void setQuestion(int question) {
        mQuestion = question;
    }

    public boolean isTrueQuestion() {
        return mTrueQuestion;
    }

    public void setTrueQuestion(boolean trueQuestion) {
        mTrueQuestion = trueQuestion;
    }
}
