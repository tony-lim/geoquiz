package com.bignerdranch.android.geoquiz;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Map;


public class QuizActivity extends Activity {

    private static final String TAG = "QuizActivity";
    private static final String KEY_INDEX = "index";
    private static final String KEY_ISCHEATER = "is_cheater";

    private boolean[] mIsCheaterList; // allocated in onCreate


    private Button mTrueButton;
    private Button mFalseButton;
    private Button mCheatButton;

    private ImageButton mNextButton;
    private ImageButton mPreviousButton;

    private TextView mQuestionTextView;
    private TextView mApiLevelTextView;


    private int mCurrentIndex = 0;
    private TrueFalse[] mQuestionBank = new TrueFalse[] {
            new TrueFalse(R.string.question_ocean, true),
            new TrueFalse(R.string.question_middleeast, false),
            new TrueFalse(R.string.question_africa, false),
            new TrueFalse(R.string.question_americas, true),
            new TrueFalse(R.string.question_asia, true),
    };

    private void updateQuestion() {
//        Log.d(TAG, "Updating question text for question #" + mCurrentIndex, new Exception());
//        Log.d(TAG, "Updating question text for question #" + mCurrentIndex);
        int question = mQuestionBank[mCurrentIndex].getQuestion();
        mQuestionTextView.setText(question); //setText takes in resourceId
    }


    private void checkAnswer(boolean userPressedTrue) {
        boolean isTrueStatement = mQuestionBank[mCurrentIndex].isTrueQuestion();
        int messageResId = 0;

        messageResId = (mIsCheaterList[mCurrentIndex] ? R.string.judgement_toast :
                (userPressedTrue == isTrueStatement ? R.string.correct_toast : R.string.incorrect_toast));
        Toast.makeText(this, messageResId, Toast.LENGTH_SHORT).show();

    }

    private void goNextQuestion() {
        mCurrentIndex = (mCurrentIndex + 1) % mQuestionBank.length;
        updateQuestion();
    }

    /**
     * Decrement the index of resourceId array, and
     * update the TextView to redisplay the question
     */
    private void goPrevQuetion() {
        mCurrentIndex = (--mCurrentIndex >= 0 ? mCurrentIndex : mQuestionBank.length + mCurrentIndex);
        updateQuestion();
    }



    @TargetApi(11)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Log.d(TAG, "onCreate(Bundle) called");
        setContentView(R.layout.activity_quiz);

        // Display api level under question
        mApiLevelTextView = (TextView) findViewById(R.id.api_level);
        mApiLevelTextView.setText( "API level " + Build.VERSION.SDK_INT);

        // wrapping sdk11 objects
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar actionBar = getActionBar();
            actionBar.setSubtitle("Bodies of water");
        }

        // transfering data between pause/unpause
        if(savedInstanceState != null) {
            mCurrentIndex = savedInstanceState.getInt(KEY_INDEX, 0);
            mIsCheaterList = savedInstanceState.getBooleanArray(KEY_ISCHEATER);
        }
        if(mIsCheaterList == null)
            mIsCheaterList = new boolean[5]; //java default boolean is false

        mQuestionTextView = (TextView)findViewById(R.id.question_text_view);
        mQuestionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goNextQuestion();
            }
        });

        mTrueButton = (Button)findViewById(R.id.true_button);
        mTrueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(true);
            }
        });

        mFalseButton = (Button)findViewById(R.id.false_button);
        mFalseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(false);
            }
        });

        mCheatButton = (Button) findViewById(R.id.cheat_button);
        mCheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // start cheatActivity
                Intent i = new Intent(QuizActivity.this, CheatActivity.class);
                boolean answerIsTrue = mQuestionBank[mCurrentIndex].isTrueQuestion();
                i.putExtra(CheatActivity.EXTRA_ANSWER_IS_TRUE, answerIsTrue);

//                startActivity(i); // this is starting activity with no feedback
                startActivityForResult(i, 0);
            }
        });

        mPreviousButton = (ImageButton) findViewById(R.id.previous_button);
        mPreviousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goPrevQuetion();
            }
        });

        mNextButton = (ImageButton)findViewById(R.id.next_button);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goNextQuestion();
            }
        });
        updateQuestion();
    }


    /**
     * This virtual functin will obtain information passed by CheatActivity to main activity
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data == null)
            return;
//        mIsCheater = data.getBooleanExtra(CheatActivity.EXTRA_ANSWER_SHOWN, false);
        mIsCheaterList[mCurrentIndex] = data.getBooleanExtra(CheatActivity.EXTRA_ANSWER_SHOWN, false);
    }


    /**
     * Save information to pass on to next unpaused activity ( ex: switch portrait/landcape mode)
     * @param savedInstanceState
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.i(TAG, "onSaveInstanceState");
        savedInstanceState.putInt(KEY_INDEX, mCurrentIndex);
//        savedInstanceState.putBoolean(KEY_ISCHEATER, mIsCheater);
        savedInstanceState.putBooleanArray(KEY_ISCHEATER, mIsCheaterList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_quiz, menu);
        return true;
    }
}


/**
 * These are just to keep track of the Activity cycles
 *
 */
//    @Override
//    public void onStart() {
//        super.onStart();
//        Log.d(TAG, "onStart() called");
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        Log.d(TAG, "onPause() called");
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        Log.d(TAG, "onResume() called");
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        Log.d(TAG, "onStop() called");
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        Log.d(TAG, "onDestroy() called");
//    }
//

